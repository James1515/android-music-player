package com.main.assignment4;

import android.animation.Animator;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    private ProgressBar NProgBar;

    private TextView LoadingStatusText;

    private int progStatus = 0;

    private Handler nHandler = new Handler();



    //Variable Declarations: Buttons 1-7: Songs
    //Buttons 8,9,10 - Settings
    Button button1, button2, button3, button4, button5, button6, button7, button8;
    Button button9, button10;

    //Declare Instances of media player:
    MediaPlayer song1, song2, song3, song4, song5, song6, song7;


    //Booleans to check is the song being played is paused or stopped:
    boolean isPaused1 = false;
    boolean isPaused2 = false;
    boolean isPaused3 = false;
    boolean isPaused4 = false;
    boolean isPaused5 = false;
    boolean isPaused6 = false;
    boolean isPaused7 = false;

    boolean isStopped1 = false;
    boolean isStopped2 = false;
    boolean isStopped3 = false;
    boolean isStopped4 = false;
    boolean isStopped5 = false;
    boolean isStopped6 = false;
    boolean isStopped7 = false;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        NProgBar = (ProgressBar) findViewById(R.id.progBar);

        LoadingStatusText = (TextView) findViewById(R.id.StatText);


        new Thread(new Runnable() {
            @Override
            public void run()
            {
                while(progStatus < 100)
                {
                    progStatus++;
                    android.os.SystemClock.sleep(70);

                    nHandler.post(new Runnable() {
                        @Override
                        public void run()
                        {
                            NProgBar.setProgress(progStatus);
                        }
                    });
                }

                nHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        LoadingStatusText.setVisibility(View.VISIBLE);
                    }
                });
            }
        }).start();

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);


        //Buttons that represent play, pause and stop
        button9 = findViewById(R.id.button9);
        button10 = findViewById(R.id.button10);


        //Seven songs that are used in the Media Player:
        song1 = MediaPlayer.create(this, R.raw.war_chant);
        song2 = MediaPlayer.create(this, R.raw.fsu_fight_song);
        song3 = MediaPlayer.create(this, R.raw.victory_song);
        song4 = MediaPlayer.create(this, R.raw.gold_and_garnett);
        song5 = MediaPlayer.create(this, R.raw.fsu_cheer);
        song6 = MediaPlayer.create(this, R.raw.seminole_uprising);
        song7 = MediaPlayer.create(this, R.raw.fourth_quarter_fanfare);


        //Buttons that are set for individual songs to be played:

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                if(isPaused1 == true)
                {
                    song1.start();
                    isPaused1 = false;
                }
                else if((isStopped1 == true) && (song1 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song1 = MediaPlayer.create(getApplicationContext(), R.raw.war_chant);
                    song1.start();

                }
                else
                {
                    song1.start();
                }



            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                if(isPaused2 == true)
                {
                    song2.start();
                    isPaused2 = false;
                }
                else if((isStopped2 == true) && (song2 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song2 = MediaPlayer.create(getApplicationContext(), R.raw.fsu_fight_song);
                    song2.start();

                }
                else
                {
                    song2.start();
                }




            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(isPaused3 == true)
                {
                    song3.start();
                    isPaused3 = false;
                }
                else if((isStopped3 == true) && (song3 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song3 = MediaPlayer.create(getApplicationContext(), R.raw.victory_song);
                    song3.start();

                }
                else
                {
                    song3.start();
                }




            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(isPaused4 == true)
                {
                    song4.start();
                    isPaused4 = false;
                }
                else if((isStopped4 == true) && (song4 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song4 = MediaPlayer.create(getApplicationContext(), R.raw.gold_and_garnett);
                    song4.start();

                }
                else
                {
                    song4.start();
                }



            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(isPaused5 == true)
                {
                    song5.start();
                    isPaused5 = false;
                }
                else if((isStopped5 == true) && (song5 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song5 = MediaPlayer.create(getApplicationContext(), R.raw.fsu_cheer);
                    song5.start();

                }
                else
                {
                    song5.start();
                }




            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(isPaused6 == true)
                {
                    song6.start();
                    isPaused6 = false;
                }
                else if((isStopped6 == true) && (song6 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song6 = MediaPlayer.create(getApplicationContext(), R.raw.seminole_uprising);
                    song6.start();

                }
                else
                {
                    song6.start();
                }




            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                if(isPaused7 == true)
                {
                    song7.start();
                    isPaused7 = false;
                }
                else if((isStopped7 == true) && (song7 == null))
                {

                    //song1.start();
                    //isStopped1 = false;
                    song7 = MediaPlayer.create(getApplicationContext(), R.raw.fourth_quarter_fanfare);
                    song7.start();

                }
                else
                {
                    song7.start();
                }

            }
        });


        //Play Button: Not needed, alwready implemented in buttons 1-7:

        //Pause Button:
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(song1.isPlaying())
                {
                    song1.pause();
                    isPaused1 = true;
                }
                else if(song2.isPlaying())
                {
                    song2.pause();
                    isPaused2 = true;
                }
                else if(song3.isPlaying())
                {
                    song3.pause();
                    isPaused3 = true;
                }
                else if(song4.isPlaying())
                {
                    song4.pause();
                    isPaused4 = true;
                }
                else if(song5.isPlaying())
                {
                    song5.pause();
                    isPaused5 = true;
                }
                else if(song6.isPlaying())
                {
                    song6.pause();
                    isPaused6 = true;
                }
                else if(song7.isPlaying())
                {
                    song7.pause();
                    isPaused7 = true;
                }

            }
        });

        //Stop Button:

        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(song1.isPlaying())
                {
                    //song1.stop();
                    isStopped1 = true;
                    song1.release();
                    song1 = null;

                }
                else if(song2.isPlaying())
                {
                    //song2.stop();
                    isStopped2 = true;
                    song2.release();
                    song2 = null;
                }
                else if(song3.isPlaying())
                {
                    //song3.stop();
                    isStopped3 = true;
                    song3.release();
                    song3 = null;
                }
                else if(song4.isPlaying())
                {
                    //song4.stop();
                    isStopped4 = true;
                    song4.release();
                    song4 = null;
                }
                else if(song5.isPlaying())
                {
                    //song5.stop();
                    isStopped5 = true;
                    song5.release();
                    song5 = null;
                }
                else if(song6.isPlaying())
                {
                    //song6.stop();
                    isStopped6 = true;
                    song6.release();
                    song6 = null;
                }
                else if(song7.isPlaying())
                {
                    //song7.stop();
                    isStopped7 = true;
                    song6.release();
                    song6 = null;
                }

            }
        });




    }//onCreate()


    //For use when the application is closed, or not in view:

    @Override
    public void onStop()
    {
        super.onStop();

        song1.pause();
        song2.pause();
        song3.pause();
        song4.pause();
        song5.pause();
        song6.pause();
        song7.pause();
    }




}
